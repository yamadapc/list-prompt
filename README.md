list-prompt
===========
A simple list prompt UI for the terminal for Haskell.

![screenshot](/screenshot.png)

## License
This code is licensed under the GPLv3 license for Pedro Tacla Yamada. For more
information, please refer to the [LICENSE](/LICENSE) file.
